<?php

/**
 * Created by PhpStorm.
 * User: Grzegorz Gąsak (info@vertoo.pl)
 * Date: 26.04.14
 */

namespace Bugsender;

class BugsenderHelpers {

    public static function full_path($qs = false) {
        $s = & $_SERVER;
        $ssl = (!empty($s['HTTPS']) && $s['HTTPS'] == 'on') ? true : false;
        $sp = strtolower($s['SERVER_PROTOCOL']);
        $protocol = substr($sp, 0, strpos($sp, '/')) . (($ssl) ? 's' : '');
        $port = $s['SERVER_PORT'];
        $port = ((!$ssl && $port == '80') || ($ssl && $port == '443')) ? '' : ':' . $port;
        $host = isset($s['HTTP_X_FORWARDED_HOST']) ? $s['HTTP_X_FORWARDED_HOST'] : (isset($s['HTTP_HOST']) ? $s['HTTP_HOST'] : null);
        $host = isset($host) ? $host : $s['SERVER_NAME'] . $port;
        $uri = $protocol . '://' . $host . $s['REQUEST_URI'];
        if ($qs) {
            return $uri;
        }
        $segments = explode('?', $uri, 2);
        $url = $segments[0];
        return $url;
    }


    public static function array_remove_key($key, & $array) {
        if (!array_key_exists($key, $array))
            return NULL;

        $val = $array[$key];
        unset($array[$key]);

        return $val;
    }


    public static function getIP() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }


    /**
     *  source: http://www.php.net/manual/en/function.apache-request-headers.php
     *
     * @return array
     */
    public static function getHeaders() {
        if (!function_exists('apache_request_headers')) {
            $arh = array();
            $rx_http = '/\AHTTP_/';
            foreach ($_SERVER as $key => $val) {
                if (preg_match($rx_http, $key)) {
                    $arh_key = preg_replace($rx_http, '', $key);
                    $rx_matches = array();
                    // do some nasty string manipulations to restore the original letter case
                    // this should work in most cases
                    $rx_matches = explode('_', $arh_key);
                    if (count($rx_matches) > 0 and strlen($arh_key) > 2) {
                        foreach ($rx_matches as $ak_key => $ak_val) $rx_matches[$ak_key] = ucfirst($ak_val);
                        $arh_key = implode('-', $rx_matches);
                    }
                    $arh[mb_convert_encoding($arh_key, 'utf-8', 'utf-8')] = mb_convert_encoding($val, 'utf-8', 'utf-8');
                }
            }
            return ($arh);
        } else {
            return apache_request_headers();
        }
    }

    /**
     * source: http://www.php.net/manual/en/function.uniqid.php#94959
     *
     * @return string
     */
    public static function uuid4() {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
                // 32 bits for "time_low"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff),

                // 16 bits for "time_mid"
                mt_rand(0, 0xffff),

                // 16 bits for "time_hi_and_version",
                // four most significant bits holds version number 4
                mt_rand(0, 0x0fff) | 0x4000,

                // 16 bits, 8 bits for "clk_seq_hi_res",
                // 8 bits for "clk_seq_low",
                // two most significant bits holds zero and one for variant DCE1.1
                mt_rand(0, 0x3fff) | 0x8000,

                // 48 bits for "node"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    public static function encodeUtf8($array) {
        if (is_array($array)) {
            foreach ($array as $k => $v) {
                if (is_array($v)) {
                    $array[$k] = self::encodeUtf8($v);
                } else {
                    $array[$k] = mb_convert_encoding($v, 'utf-8', 'utf-8');
                }
            }
            return $array;
        }
        return $array;
    }
} 