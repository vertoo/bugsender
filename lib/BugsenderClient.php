<?php
/**
 * Created by PhpStorm.
 * User: Grzegorz Gąsak (info@vertoo.pl)
 * Date: 26.04.14
 */
namespace Bugsender;

use Bugsender\BugsenderHelpers;

/**
 * Class Bugsender_Client
 * @package Bugsender
 */
class BugsenderClient {

    /**
     * @var string host API
     */
    private $apiHost;
    /**
     * @var string path API
     */
    private $apiPath;


    /**
     * @var array message array to send
     */
    private $message;


    /**
     * @var array keys not send with $_POST data
     */
    private $stop_keys = array(
            'pass',
            'password',
            'confirm_password',
            'password_confirm',
            'password_confirmation',
    );

    const ERROR = 'error';
    const WARNING = 'warning';
    const INFO = 'info';


    /**
     * @var int how much lines of code
     */
    private $code_length = 10;

    /**
     * @var string private key
     */
    private $privateKey;

    /**
     * @var string public key
     */
    private $publicKey;

    /**
     * @var string version of application
     */
    private $version;


    /**
     * @var array additional data in array
     */
    private $additional_data;


    /**
     * @param $privateKey
     * @param $publicKey
     */
    public function __construct($privateKey, $publicKey) {
        $this->message = array();

//         $this->apiHost = 'http://bugsmonitor-app.dev/';
		$this->apiHost = 'http://app.bugsmonitor.com/';
        $this->apiPath = 'api/report_bug';

        $this->privateKey = $privateKey;
        $this->publicKey = $publicKey;

        $this->version = '';
    }


    /**
     * @param string $type    error type
     * @param string $message error message
     * @param string $file    filename with error
     * @param int    $line    line in file
     * @param        $PHP_ERROR
     * @param  object   string $exception
     *
     * @internal param string $object $trace
     */
    private function buildMessage($type, $message, $file, $line, $PHP_ERROR, $exception = '') {
        $trace = ($PHP_ERROR AND !is_object($exception)) ? array_slice(debug_backtrace(), 1) : $exception->getTrace();
        $source = ($file !== null AND $line !== null) ? $this->getCode($file, $line) : '';

        $this->message = array(
                'uuid'       => mb_convert_encoding(BugsenderHelpers::uuid4(), 'utf-8', 'utf-8'),
                'date'       => gmdate('Y-m-d\TH:i:s\Z'),
                'type'       => mb_convert_encoding($this->translateError($type), 'utf-8', 'utf-8'),
                'url'        => mb_convert_encoding(BugsenderHelpers::full_path(true), 'utf-8', 'utf-8'),
                'file'       => mb_convert_encoding($file, 'utf-8', 'utf-8'),
                'line'       => mb_convert_encoding($line, 'utf-8', 'utf-8'),
                'message'    => mb_convert_encoding($message, 'utf-8', 'utf-8'),
                'trace'      => $trace,
                'session'    => session_id(),
                'env'        => array(
                        'version'      => mb_convert_encoding($this->version, 'utf-8', 'utf-8'),
                        'lang'         => 'php',
                        'lang_version' => PHP_VERSION,
                ),
                'request'    => array(
                        'request_type' => array_key_exists('REQUEST_METHOD', $_SERVER) ? mb_convert_encoding($_SERVER['REQUEST_METHOD'], 'utf-8', 'utf-8') : false,
                        'ip'           => BugsenderHelpers::getIp(),
                        'ua'           => array_key_exists('HTTP_USER_AGENT', $_SERVER) ? mb_convert_encoding($_SERVER['HTTP_USER_AGENT'], 'utf-8', 'utf-8') : false,
                        'headers'      => BugsenderHelpers::getHeaders(),
                        'query_string' => array_key_exists('QUERY_STRING', $_SERVER) ? mb_convert_encoding($_SERVER['QUERY_STRING'], 'utf-8', 'utf-8') : false,
                        'post'         => false,
                        'cookie'       => !empty($_COOKIE) ? BugsenderHelpers::encodeUtf8($_COOKIE) : false,

                ),
                'additional' => BugsenderHelpers::encodeUtf8($this->additional_data),

                'is_cli'     => php_sapi_name() == 'cli' ? 1 : 0,
                'is_fatal'   => in_array($type, array(E_ERROR, E_CORE_ERROR, E_COMPILE_ERROR, E_PARSE)) ? 1 : 0,
                'code'       => $source,
        );

        if (isset($_POST) AND is_array($_POST) AND count($_POST) > 0) {
            $post = $_POST;
            // remove from request some data like passwords, cc_number etc.
            foreach ($this->stop_keys as $k) {
                BugsenderHelpers::array_remove_key($k, $post);
            }
            $this->message['request']['post'] = BugsenderHelpers::encodeUtf8($post);
            unset($post);
        }
    }

    /**
     * @param $error int
     * @return string
     */
    private function translateError($error) {
        switch ($error) {
            case E_ERROR:
            case E_PARSE:
            case E_CORE_ERROR:
            case E_COMPILE_ERROR:
            case E_USER_ERROR:
            case E_RECOVERABLE_ERROR:
                return self::ERROR;

            case E_WARNING:
            case E_CORE_WARNING:
            case E_COMPILE_WARNING:
            case E_USER_WARNING:
                return self::WARNING;

            case E_NOTICE:
            case E_USER_NOTICE:
            case E_STRICT:
                return self::INFO;
        }

        if (version_compare(PHP_VERSION, '5.3.0', '>=')) {
            if ($error === E_DEPRECATED OR $error === E_USER_DEPRECATED) {
                return self::WARNING;
            }
        }
        return self::ERROR;
    }

    /**
     * @param $file string Filename
     * @param $line int Line number
     * @return string
     */
    private function getCode($file, $line) {
        if (file_exists($file)) {
            $file_content = file_get_contents($file);
            $start = $line - floor($this->code_length / 2);
            if ($start < 0) {
                $start = 0;
            }
            $lines = explode("\n", $file_content);
            return array_slice($lines, $start, $this->code_length, true);
        }
        return '';
    }


    /**
     * @param $type
     * @param $message
     * @param $file
     * @param $line
     * @param bool $PHP_ERROR
     * @param string $exception
     */
    public function send($type, $message, $file, $line, $PHP_ERROR, $exception = '') {

        $this->buildMessage($type, $message, $file, $line, $PHP_ERROR, $exception);

        $sData = json_encode($this->message);

        $hash = hash_hmac('sha256', $sData, $this->privateKey);
        $headers = array(
                'X-Public: ' . $this->publicKey,
                'X-Hash: ' . $hash,
                'Content-Type: application/json',
                'Content-Length: ' . strlen($sData)
        );

        $ch = curl_init($this->apiHost . $this->apiPath);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);


        curl_setopt($ch, CURLOPT_POSTFIELDS, $sData);
        $result = curl_exec($ch);

        curl_close($ch);

        $this->message = array();
    }


    /**
     * @param array $stop_keys
     */
    public function setStopKeys($stop_keys) {
        $this->stop_keys = $stop_keys;
    }

    /**
     * @param mixed $stop_keys
     */
    public function addStopKeys($stop_keys) {
        if (!is_array($stop_keys)) {
            array_push($this->stop_keys, $stop_keys);
        } else {
            $this->stop_keys = array_merge($this->stop_keys, $stop_keys);
        }
    }

    /**
     * @param mixed $version
     */
    public function setVersion($version) {
        $this->version = $version;
    }

    /**
     * @param array $additional_data
     */
    public function setAdditionalData($additional_data) {
        $this->additional_data = $additional_data;
    }

    /**
     * @param string $apiHost
     */
    public function setApiHost($apiHost)
    {
        $this->apiHost = $apiHost;
    }

} 